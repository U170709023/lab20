public class GCDLoop{
    public static void main(String[] args){
        int x = Integer.parseInt(args[0]);
        int y = Integer.parseInt(args[1]);

        System.out.println(gcd(x,y));
    }
       public static int gcd(int x , int y) {
         int r = 0;
         while(y !=0) {
            r = x % y;
            x = y;
            y = r;
       }
       return x;
    }
}
