public class GCDRec{
    public static void main(String[] args){
        int x = Integer.parseInt(args[0]);
        int y = Integer.parseInt(args[1]);

        System.out.println(gcd(x,y));
    }
       public static int gcd(int x , int y) {
         return (x % y == 0) ? y : gcd(y, x % y); 
    }
}
